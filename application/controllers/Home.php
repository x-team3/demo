<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('migration','session');
		$this->load->helper('url');
	}

	public function index($page = 'Home')
	{
		if ( ! file_exists(APPPATH.'views/Pages/'.$page.'.php'))
		{
			show_404();
		}
		// $data['title'] = ucfirst($page);
		$this->load->view('Template/Header');
		$this->load->view('Pages/'.$page);
		$this->load->view('Template/Footer');
	}

	public function print($my_view)
	{
		$my_view = str_replace(".","/",$my_view);
		$html = $this->load->view($my_view,[],true);
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->SetHeader('Document Title|Center Text|{PAGENO}');
		$mpdf->SetFooter('Document Title');
		$mpdf->defaultheaderfontsize=10;
		$mpdf->defaultheaderfontstyle='B';
		$mpdf->defaultheaderline=0;
		$mpdf->defaultfooterfontsize=10;
		$mpdf->defaultfooterfontstyle='BI';
		$mpdf->defaultfooterline=0;
		$mpdf->WriteHTML($html);
		$mpdf->Output();
	}
}

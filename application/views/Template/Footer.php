		<footer class="footer">
			<div class="container p-0">
				<div class="row justify-content-around p-0">
					<div class="col-lg-6 col-md-6 col-sm-12 text-center	 d-flex justify-content-lg-start justify-content-md-start justify-content-sm-center align-items-center py-1">
						<span class="footer-copyright">&copy; Copyright 2021 <a href="" target="_blank">RHA</a></span>
					</div>

					<div class="col-lg-6 col-md-6 col-sm-12 text-center d-flex justify-content-lg-end justify-content-md-end justify-content-sm-center align-items-center py-1">
						<a href="" class="footer-icon facebook"><i class="fab fa-facebook"></i></a>
						<a href=""  class="footer-icon paper"><i class="far fa-paper-plane"></i></a>
						<a href=""  class="footer-icon twitter"><i class="fab fa-twitter-square"></i></a>
					</div>
				</div>
			</div>
		</footer>
		<script src="<?=base_url();?>assets\Home\js\jquery-3.5.1.slim.min.js"></script>
		<script src="<?=base_url();?>assets\Home\js\popper.min.js"></script>
		<script src="<?=base_url();?>assets\Home\js\bootstrap.min.js"></script>
		<script src="<?=base_url();?>assets\Home\js\tinymce.min.js"></script>
		<script>tinymce.init({selector:'textarea'});</script>
	</body>
</html>

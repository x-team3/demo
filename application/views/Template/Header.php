<!doctype html>
<html lang="en">
	<head>
		<title>Our Demo</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link type="text/css" rel="stylesheet" href="<?=base_url();?>assets\Home\css\bootstrap.min.css">
		<link type="text/css" rel="stylesheet" href="<?=base_url();?>assets\Home\css\all.min.css">
		<link type="text/css" rel="stylesheet" href="<?=base_url();?>assets\Home\css\style.css">
	</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-light navbar-bg sticky-top">
			<div class="container">
				<a class="navbar-brand" href="">RHS</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
					<div class="navbar-nav ml-auto">
						<a class="nav-link nav-link-color text-center" id="Home" href="<?=base_url();?>Home">Add User<span class="sr-only">(current)</span></a>
						<a class="nav-link nav-link-color text-center" href="#">Users List</a>
						<a class="nav-link nav-link-color text-center" href="#">Update User</a>
					</div>
				</div>
			</div>
		</nav>

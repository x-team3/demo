	<?php
		$my_view =  $_ci_view;
		$my_view = str_replace("/",".",$my_view);
	?>

	<section class="container my-5">
		<div class="row justify-content-center">
			<form class="border px-5 pb-5 rounded shadow w-50" method="post" action="<?=base_url();?>Home\add_user">
				<h3 class="text-center pt-2">Add User</h3>
				<hr>
				<div class="form-group">
					<label for="userName">User Name</label>
					<input type="text" class="form-control" id="userName" placeholder="Enter User Name" name="userName" required>
				</div>
				<div class="form-group">
					<label for="Email">Email</label>
					<input type="email" class="form-control" id="Email" placeholder="Enter Email" name="userEmail" required>
					<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" id="password" placeholder="Enter Password" name="userPassword" required>
				</div>
				<div class="form-group">
					<label for="description">Description</label>
					<textarea class="form-control" id="description" placeholder="Enter Description < optional >" name="userDescription"></textarea>
				</div>
				<button type="submit" class="btn btn-outline-success">Add</button>
				<a href="<?=base_url();?>Home\print\<?=$my_view;?>" target="_blank" class="btn btn-outline-info rounded float-right">print view</a>
			</form>

		</div>
	</section>

